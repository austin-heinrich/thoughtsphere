from django.apps import AppConfig


class ThoughtsphereConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'thoughtsphere'
