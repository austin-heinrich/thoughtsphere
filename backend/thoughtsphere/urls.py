from django.urls import path
from . import views

# application urls

urlpatterns = [
    path('', views.home),
]