// config file for prettier

module.exports = {
  trailingComma: 'never',
  tabWidth: 2,
  semi: false,
  singleQuote: true,
};
